import React, {useEffect, useState} from "react";
import EditableTable from "../SmallComponents/EditableTable";
import fieldsForTable from "../functions";
import CommonService from "../CommonService";
import '../MainComponents/Log.css'

const schedul = [
    {id: 1, zone: "Cucumbers", 'last watering time': "5AM", 'last temperature check': '5AM', 'last temperature data': '23 C', 'last illuminance check': '5AM', 'last illuminance data': '4 kilolux', 'last ventilation time': '3AM'},
    {id: 2, zone: "Tomatoes", 'last watering time': "5AM", 'last temperature check': '5AM', 'last temperature data': '23 C', 'last illuminance check': '5AM', 'last illuminance data': '5 kilolux', 'last ventilation time': '4AM'},
    {id: 3, zone: "Strawberry", 'last watering time': "5AM", 'last temperature check': '5AM', 'last temperature data': '22 C', 'last illuminance check': '5AM', 'last illuminance data': '4 kilolux', 'last ventilation time': '1AM'},
    {id: 4, zone: "Cranberry", 'last watering time': "5AM", 'last temperature check': '5AM', 'last temperature data': '23 C', 'last illuminance check': '5AM', 'last illuminance data': '4 kilolux', 'last ventilation time': '3AM'},
    {id: 5, zone: "Eggplant", 'last watering time': "5AM", 'last temperature check': '5AM', 'last temperature data': '23 C', 'last illuminance check': '5AM', 'last illuminance data': '4 kilolux', 'last ventilation time': '3AM'},
    {id: 6, zone: "Potato", 'last watering time': "5AM", 'last temperature check': '5AM', 'last temperature data': '23 C', 'last illuminance check': '5AM', 'last illuminance data': '4 kilolux', 'last ventilation time': '3AM'},
    {id: 7, zone: "Cranberry", 'last watering time': "5AM", 'last temperature check': '5AM', 'last temperature data': '23 C', 'last illuminance check': '5AM', 'last illuminance data': '4 kilolux', 'last ventilation time': '3AM'},
    {id: 8, zone: "Tomatoes", 'last watering time': "5AM", 'last temperature check': '5AM', 'last temperature data': '23 C', 'last illuminance check': '5AM', 'last illuminance data': '4 kilolux', 'last ventilation time': '3AM'},
    {id: 9, zone: "Eggplant", 'last watering time': "5AM", 'last temperature check': '5AM', 'last temperature data': '23 C', 'last illuminance check': '5AM', 'last illuminance data': '4 kilolux', 'last ventilation time': '3AM'},
    {id: 10, zone: "Strawberry", 'last watering time': "5AM", 'last temperature check': '5AM', 'last temperature data': '23 C', 'last illuminance check': '5AM', 'last illuminance data': '4 kilolux', 'last ventilation time': '3AM'},
    {id: 11, zone: "Cucumbers", 'last watering time': "5AM", 'last temperature check': '5AM', 'last temperature data': '23 C', 'last illuminance check': '5AM', 'last illuminance data': '4 kilolux', 'last ventilation time': '3AM'},
    {id: 12, zone: "Cranberry", 'last watering time': "5AM", 'last temperature check': '5AM', 'last temperature data': '23 C', 'last illuminance check': '5AM', 'last illuminance data': '4 kilolux', 'last ventilation time': '3AM'},
    {id: 13, zone: "Tomatoes", 'last watering time': "5AM", 'last temperature check': '5AM', 'last temperature data': '23 C', 'last illuminance check': '5AM', 'last illuminance data': '4 kilolux', 'last ventilation time': '3AM'},
    {id: 14, zone: "Eggplant", 'last watering time': "5AM", 'last temperature check': '5AM', 'last temperature data': '23 C', 'last illuminance check': '5AM', 'last illuminance data': '4 kilolux', 'last ventilation time': '3AM'},
    {id: 15, zone: "Strawberry", 'last watering time': "5AM", 'last temperature check': '5AM', 'last temperature data': '23 C', 'last illuminance check': '5AM', 'last illuminance data': '4 kilolux', 'last ventilation time': '3AM'},
    {id: 16, zone: "Cucumbers", 'last watering time': "5AM", 'last temperature check': '5AM', 'last temperature data': '23 C', 'last illuminance check': '5AM', 'last illuminance data': '4 kilolux', 'last ventilation time': '3AM'},
    {id: 17, zone: "Cranberry", 'last watering time': "5AM", 'last temperature check': '5AM', 'last temperature data': '23 C', 'last illuminance check': '5AM', 'last illuminance data': '4 kilolux', 'last ventilation time': '3AM'},];
let fields = fieldsForTable(schedul);


function Schedule() {
    const [schedule, setSchedule] = useState(schedul);

    useEffect(()=>{
        CommonService.getAll('/log')
            .then(response => {
                setSchedule(response.data);
                fields = fieldsForTable(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    })
    return (
        <>
            <EditableTable
                rows={schedule}
                fields={fields}
                isEditable={false}
                onRowEdit={(row)=>{console.log(row)}}
                className="Table"
            />
        </>
    )
}

export default Schedule;
