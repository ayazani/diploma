import React, {useEffect, useState} from "react";
import EditableTable from "../SmallComponents/EditableTable";
import fieldsForTable from "../functions";
import './Zones.css'
import CommonService from "../CommonService";
import EditIcon from '@material-ui/icons/Edit';
import { IconButton } from "@material-ui/core";
import Modalochka from "../SmallComponents/Modal";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const zone = {
        cucumbers:[
            {
                id: 1,
                sensor: 'temperature',
                data: '25 C',
                date: '20.06.21',
                time: '9AM'
            },
            {
                id: 2,
                sensor: 'illuminance',
                data: '5 kilolux',
                date: '20.06.21',
                time: '9AM'
            },
            {
                id: 3,
                sensor: 'air humidity',
                data: '55%',
                date: '20.06.21',
                time: '9AM'
            },
            {
                id: 4,
                sensor: 'temperature',
                data: '20 C',
                date: '20.06.21',
                time: '10AM'
            },
            {
                id: 5,
                sensor: 'illuminance',
                data: '5 kilolux',
                date: '20.06.21',
                time: '10AM'
            },
            {
                id: 6,
                sensor: 'air humidity',
                data: '60%',
                date: '20.06.21',
                time: '10AM'
            },
            {
                id: 7,
                sensor: 'temperature',
                data: '22 C',
                date: '20.06.21',
                time: '11AM'
            },
            {
                id: 8,
                sensor: 'temperature',
                data: '24 C',
                date: '20.06.21',
                time: '12AM'
            },
            {
                id: 9,
                sensor: 'temperature',
                data: '19 C',
                date: '20.06.21',
                time: '12AM'
            },
            {
                id: 10,
                sensor: 'temperature',
                data: '21 C',
                date: '20.06.21',
                time: '12AM'
            }
        ],
    strawberry:[
        {
            id: 1,
            sensor: 'temperature',
            data: '25 C',
            date: '20.06.21',
            time: '9AM'
        },
        {
            id: 2,
            sensor: 'illuminance',
            data: '5 kilolux',
            date: '20.06.21',
            time: '9AM'
        },
        {
            id: 3,
            sensor: 'air humidity',
            data: '55%',
            date: '20.06.21',
            time: '9AM'
        },
        {
            id: 4,
            sensor: 'temperature',
            data: '20 C',
            date: '20.06.21',
            time: '10AM'
        },
        {
            id: 5,
            sensor: 'illuminance',
            data: '5 kilolux',
            date: '20.06.21',
            time: '10AM'
        },
        {
            id: 6,
            sensor: 'air humidity',
            data: '60%',
            date: '20.06.21',
            time: '10AM'
        },
        {
            id: 7,
            sensor: 'temperature',
            data: '22 C',
            date: '20.06.21',
            time: '11AM'
        },
        {
            id: 8,
            sensor: 'temperature',
            data: '24 C',
            date: '20.06.21',
            time: '12AM'
        },
        {
            id: 9,
            sensor: 'temperature',
            data: '19 C',
            date: '20.06.21',
            time: '12AM'
        },
        {
            id: 10,
            sensor: 'temperature',
            data: '21 C',
            date: '20.06.21',
            time: '12AM'
        }
    ],
    cranberry:[
        {
            id: 1,
            sensor: 'temperature',
            data: '25 C',
            date: '20.06.21',
            time: '9AM'
        },
        {
            id: 2,
            sensor: 'illuminance',
            data: '5 kilolux',
            date: '20.06.21',
            time: '9AM'
        },
        {
            id: 3,
            sensor: 'air humidity',
            data: '55%',
            date: '20.06.21',
            time: '9AM'
        },
        {
            id: 4,
            sensor: 'temperature',
            data: '20 C',
            date: '20.06.21',
            time: '10AM'
        },
        {
            id: 5,
            sensor: 'illuminance',
            data: '5 kilolux',
            date: '20.06.21',
            time: '10AM'
        },
        {
            id: 6,
            sensor: 'air humidity',
            data: '60%',
            date: '20.06.21',
            time: '10AM'
        },
        {
            id: 7,
            sensor: 'temperature',
            data: '22 C',
            date: '20.06.21',
            time: '11AM'
        },
        {
            id: 8,
            sensor: 'temperature',
            data: '24 C',
            date: '20.06.21',
            time: '12AM'
        },
        {
            id: 9,
            sensor: 'temperature',
            data: '19 C',
            date: '20.06.21',
            time: '12AM'
        },
        {
            id: 10,
            sensor: 'temperature',
            data: '21 C',
            date: '20.06.21',
            time: '12AM'
        }
    ],
    tomatoes:[
        {
            id: 1,
            sensor: 'temperature',
            data: '25 C',
            date: '20.06.21',
            time: '9AM'
        },
        {
            id: 2,
            sensor: 'illuminance',
            data: '5 kilolux',
            date: '20.06.21',
            time: '9AM'
        },
        {
            id: 3,
            sensor: 'air humidity',
            data: '55%',
            date: '20.06.21',
            time: '9AM'
        },
        {
            id: 4,
            sensor: 'temperature',
            data: '20 C',
            date: '20.06.21',
            time: '10AM'
        },
        {
            id: 5,
            sensor: 'illuminance',
            data: '5 kilolux',
            date: '20.06.21',
            time: '10AM'
        },
        {
            id: 6,
            sensor: 'air humidity',
            data: '60%',
            date: '20.06.21',
            time: '10AM'
        },
        {
            id: 7,
            sensor: 'temperature',
            data: '22 C',
            date: '20.06.21',
            time: '11AM'
        },
        {
            id: 8,
            sensor: 'temperature',
            data: '24 C',
            date: '20.06.21',
            time: '12AM'
        },
        {
            id: 9,
            sensor: 'temperature',
            data: '19 C',
            date: '20.06.21',
            time: '12AM'
        },
        {
            id: 10,
            sensor: 'temperature',
            data: '21 C',
            date: '20.06.21',
            time: '12AM'
        }
    ],
    potato:[
        {
            id: 1,
            sensor: 'temperature',
            data: '25 C',
            date: '20.06.21',
            time: '9AM'
        },
        {
            id: 2,
            sensor: 'illuminance',
            data: '5 kilolux',
            date: '20.06.21',
            time: '9AM'
        },
        {
            id: 3,
            sensor: 'air humidity',
            data: '55%',
            date: '20.06.21',
            time: '9AM'
        },
        {
            id: 4,
            sensor: 'temperature',
            data: '20 C',
            date: '20.06.21',
            time: '10AM'
        },
        {
            id: 5,
            sensor: 'illuminance',
            data: '5 kilolux',
            date: '20.06.21',
            time: '10AM'
        },
        {
            id: 6,
            sensor: 'air humidity',
            data: '60%',
            date: '20.06.21',
            time: '10AM'
        },
        {
            id: 7,
            sensor: 'temperature',
            data: '22 C',
            date: '20.06.21',
            time: '11AM'
        },
        {
            id: 8,
            sensor: 'temperature',
            data: '24 C',
            date: '20.06.21',
            time: '12AM'
        },
        {
            id: 9,
            sensor: 'temperature',
            data: '19 C',
            date: '20.06.21',
            time: '12AM'
        },
        {
            id: 10,
            sensor: 'temperature',
            data: '21 C',
            date: '20.06.21',
            time: '12AM'
        }
    ],
    eggplant : [
        {
            id: 1,
            sensor: 'temperature',
            data: '25 C',
            date: '20.06.21',
            time: '9AM'
        },
        {
            id: 2,
            sensor: 'illuminance',
            data: '5 kilolux',
            date: '20.06.21',
            time: '9AM'
        },
        {
            id: 3,
            sensor: 'air humidity',
            data: '55%',
            date: '20.06.21',
            time: '9AM'
        },
        {
            id: 4,
            sensor: 'temperature',
            data: '20 C',
            date: '20.06.21',
            time: '10AM'
        },
        {
            id: 5,
            sensor: 'illuminance',
            data: '5 kilolux',
            date: '20.06.21',
            time: '10AM'
        },
        {
            id: 6,
            sensor: 'air humidity',
            data: '60%',
            date: '20.06.21',
            time: '10AM'
        },
        {
            id: 7,
            sensor: 'temperature',
            data: '22 C',
            date: '20.06.21',
            time: '11AM'
        },
        {
            id: 8,
            sensor: 'temperature',
            data: '24 C',
            date: '20.06.21',
            time: '12AM'
        },
        {
            id: 9,
            sensor: 'temperature',
            data: '19 C',
            date: '20.06.21',
            time: '12AM'
        },
        {
            id: 10,
            sensor: 'temperature',
            data: '21 C',
            date: '20.06.21',
            time: '12AM'
        }
    ]
    }


function Zones() {
    const [zones, setZones] = useState(zone);
    const [editableZone, setEditableZone] = useState({});
    const [open, setOpen] = useState(false);

    const updateZoneName = () =>{
        CommonService.update('/zones', editableZone.id, editableZone.name)
            .then(response=>{
                console.log("res: "+response)
            })
            .catch(error => {
                console.log('ERROR: ' + error);
            })
        handleClose();
    }

    const body = (
        <div>
            <h2 id="simple-modal-title">Edit zone name</h2>
            <form noValidate autoComplete="off" className="ModalBody">
                <TextField  label="Name" value={editableZone.name} onChange={(e) => {
                    setEditableZone({id: editableZone.id, name: e.target.value})
                }} id="Input"/>
                <Button variant="outlined" color="secondary" onClick={updateZoneName}>
                    Save
                </Button>
            </form>
        </div>
    );

    const handleOpen = () => {

        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const editZone = (id, name) => {
        handleOpen();
        setEditableZone({id: id, name: name})
    }

        useEffect(()=>{
        CommonService.getAll('/zones')
            .then(response => {
                setZones(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    });


    return (
        <>
        <div className="Zones">
            {Object.entries(zones).map(zone => (
                <div className="Table">
                    <div className="Name">{zone[0]}
                        <IconButton aria-label="delete">
                            <EditIcon fontSize="small" onClick={()=>{editZone(zone[1]["0"].id, zone[0])}}/>
                        </IconButton>
                    </div>
                    <EditableTable
                        rows={(zone[1])}
                        fields={fieldsForTable(zone[1])}
                        isEditable={false}
                    />
                </div>
            ))}
        </div>
           <Modalochka handleClose={handleClose} handleOpen={handleOpen} body={body} open={open}/>
            </>
    )
}

export default Zones;
