import React, {useEffect, useState} from "react";
import TemperatureChart from "../SmallComponents/Charts/Temperature";
import AirHumidityChart from "../SmallComponents/Charts/AirHumidity";
import DatePicker from "../SmallComponents/DatePicker";
import IlluminanceChart from "../SmallComponents/Charts/Illuminance";
import "./Home.css"
import Snackbar from '@material-ui/core/Snackbar';
import CommonService from "../CommonService";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from '@material-ui/icons/Close';
const dat =
    {
        temperature: [
            {
                time: "9AM",
                "temperature °C": 16
            },
            {
                time: "10AM",
                "temperature °C": 30
            },
            {
                time: "11AM",
                "temperature °C": 20
            },
            {
                time: "12AM",
                "temperature °C": 14
            },
            {
                time: "1PM",
                "temperature °C": 22
            },
            {
                time: "2PM",
                "temperature °C": 27
            },
            {
                time: "3PM",
                "temperature °C": 19
            }
        ],
        humidity: [
            {
                time: "9AM",
                "humidity%": 30
            },
            {
                time: "10AM",
                "humidity%": 45
            },
            {
                time: "11AM",
                "humidity%": 55
            },
            {
                time: "12AM",
                "humidity%": 60
            },
            {
                time: "1PM",
                "humidity%": 75
            },
            {
                time: "2PM",
                "humidity%": 65
            },
            {
                time: "3PM",
                "humidity%": 58
            }
        ],
        illuminance: [
            {
                time: "9AM",
                kilolux: 3
            },
            {
                time: "10AM",
                kilolux: 5
            },
            {
                time: "11AM",
                kilolux: 6
            },
            {
                time: "12AM",
                kilolux: 6
            },
            {
                time: "1PM",
                kilolux: 9
            },
            {
                time: "2PM",
                kilolux: 7
            },
            {
                time: "3PM",
                kilolux: 6
            }
        ]
    }


 function Home() {
     const [dateIsDefault, setIsDefault] = useState(true);
     const [data, setData] = useState(dat);
     const [date, setDate] = useState(new Date());
     const [isOpen, setIsOpen] = useState(false);

     const onDateChange = (date) => {
         setDate(date);
         setIsDefault(false);
         CommonService.get('/home', date)
             .then(response =>{
                 setData(response.data);
         })
             .catch(e => {
                 console.log(e);
                 setIsOpen(true);
             })
     }

     useEffect(()=>{
         if (dateIsDefault){
             onDateChange(date);
         }
     })

     const handleClose = () => {
         setIsOpen(false);
     };

     return (
         <div className="Home">
             <Snackbar
                 anchorOrigin={{
                     vertical: 'top',
                     horizontal: 'center',
                 }}
                 open={isOpen}
                 onClose={handleClose}
                 message="No data for chosen day"
                 key={"vertical + horizontal"}
                 action={
                     <IconButton
                         aria-label="close"
                         color="inherit"
                         onClick={handleClose}>
                         <CloseIcon/>
                     </IconButton>
                 }
             />
             <div  className= "DatePicker">
                <DatePicker isTimePickerNeeded={false} isModal={true} date={date} onDateChange={(date)=>{onDateChange(date)}}/>
             </div>
             <div className="Graphics">
                 <TemperatureChart data={data.temperature} width={500} height={500}/>
                 <AirHumidityChart data={data.humidity} width={500} height={500}/>
                 <IlluminanceChart data={data.illuminance} width={500} height={500} />
             </div>
         </div>
     )
}

export default Home;
