import React, {useEffect, useState} from "react";
import EditableTable from "../SmallComponents/EditableTable";
import fieldsForTable from "../functions";
import CommonService from "../CommonService";

const row = [
    {id:1, name: 'Cupcake', calories: 305, fat:3.7},
    {id:2, name: 'Cupcake', calories: 305, fat:3.7},
    {id:3, name: 'Cupcake', calories: 305, fat:3.7},
    {id:4, name: 'Cupcake', calories: 305, fat:3.7},
    {id:5, name: 'Cupcake', calories: 305, fat:3.7},
    {id:6, name: 'Cupcake', calories: 305, fat:3.7},
    {id:7, name: 'Cupcake', calories: 305, fat:3.7},
    {id:8, name: 'Cupcake', calories: 305, fat:3.7},
    {id:9, name: 'Cupcake', calories: 305, fat:3.7},
    {id:10, name: 'Cupcake', calories: 305, fat:3.7},
    {id:11, name: 'Cupcake', calories: 305, fat:3.7},
    {id:12, name: 'Cupcake', calories: 305, fat:3.7},
];
let fields = fieldsForTable(row);


function Info() {
    const [rows, setRows] = useState(row);

    useEffect(()=>{
        CommonService.getAll('/sensortypes')
            .then(response => {
                setRows(response.data);
                fields = fieldsForTable(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    });

    return (
        <>
            <EditableTable
                rows={rows}
                fields={fields}
                isEditable={false}
            />
        </>
    )
}

export default Info;
