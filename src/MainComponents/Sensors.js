import React, {useEffect, useState} from "react";
import EditableTable from "../SmallComponents/EditableTable";
import Button from "@material-ui/core/Button";
import "./Settings.css"
import fieldsForTable from "../functions";
import CommonService from "../CommonService";
import Modalochka from "../SmallComponents/Modal";
import TextField from '@material-ui/core/TextField';
import AddIcon from '@material-ui/icons/Add';
import IconButton from "@material-ui/core/IconButton";
import './Sensors.css';
import Select from '@material-ui/core/Select';
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";

const sens = [
    {id: 1, zone: 'cucumbers', type: 'thermometer', IP: '187.874.9.7', enabled: 'yes'},
    {id: 2, zone: 'cucumbers', type: 'luxmeter', IP: '187.874.9.6', enabled: 'yes'},
    {id: 3, zone: 'cucumbers', type: 'air humidity detector', IP: '187.874.9.5', enabled: 'yes'},
    {id: 4, zone: 'strawberry', type: 'thermometer', IP: '187.874.8.7', enabled: 'yes'},
    {id: 5, zone: 'strawberry', type: 'luxmeter', IP: '187.874.9.6', enabled: 'yes'},
    {id: 6, zone: 'strawberry', type: 'air humidity detector', IP: '187.874.9.8', enabled: 'yes'},
    {id: 4, zone: 'eggplant', type: 'thermometer', IP: '187.874.9.7', enabled: 'yes'},
    {id: 5, zone: 'eggplant', type: 'luxmeter', IP: '187.874.5.6', enabled: 'yes'},
    {id: 6, zone: 'eggplant', type: 'air humidity detector', IP: '187.874.6.5', enabled: 'yes'},

];

let lastId = 6;

function Sensors () {

    const [sensors, setSensors] = useState(sens);
    const [open, setOpen] = useState(false);
    const [IP, setIP] = useState('');
    const [Type, setType] = useState('');
    const [Zone, setZone] = useState('');

    const onIpChange = (IP) => {
        setIP(IP);
    }

    const onZoneChange = (Zone) => {
        setZone(Zone);
    }

    const onTypeChange = (Type) => {
        setType(Type);
    }


    const createSensor = () => {
        const sensor = {name: Zone, type: Type, ip: IP}
        lastId = lastId+1;
        CommonService.create('/addSensor',  sensor)
            .then(response=>{
                console.log("res: "+response)
            })
            .catch(error => {
                console.log('ERROR: ' + error);
            })
        handleClose();
    }

    const body = (
        <div>
            <h2 id="simple-modal-title" className={"modal"}>Add new sensor</h2>
            <form noValidate autoComplete="off" className="ModalBody">
                <TextField  label="IP" value={IP} onChange={(e) => {
                    onIpChange(e.target.value)
                }} id="Input" className={"inputs"} />
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={Zone}
                    onChange={(e) => {
                        onZoneChange(e.target.value)
                    }}
                    className={"inputs"}
                >
                    <MenuItem value={'cucumbers'}>Cucumbers</MenuItem>
                    <MenuItem value={'strawberry'}>Strawberry</MenuItem>
                    <MenuItem value={'eggplant'}>Eggplant</MenuItem>
                    <MenuItem value={'potato'}>Potato</MenuItem>
                    <MenuItem value={'cranberry'}>Cranberry</MenuItem>
                    <MenuItem value={'tomatoes'}>Tomatoes</MenuItem>
                </Select>
                <TextField  label="Type" value={Type} onChange={(e) => {
                    onTypeChange(e.target.value)
                }} id="Input" className={"inputs"}/>
                <Button variant="outlined" color="secondary" onClick={createSensor} className={"button-save"}>
                    Save
                </Button>
            </form>
            <FormHelperText>Format 123.456.7.8.</FormHelperText>
        </div>
    );

    const handleOpen = () => {

        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
        setIP('');
        setType('');
        setZone('');
    };


    useEffect(()=>{
        CommonService.getAll('/sensors')
            .then(response => {
                setSensors(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    });

    return(
        <>
            <IconButton aria-label="delete">
                <AddIcon fontSize="large" onClick={handleOpen}/>
            </IconButton>
            <EditableTable
                rows={sensors}
                fields={fieldsForTable(sensors)}
                isEditable={false}
            />
            <Modalochka
                handleClose={handleClose}
                handleOpen={handleOpen}
                body={body} open={open}
                width={600}
                height={200}
            />
        </>
    );
}

export default Sensors;
