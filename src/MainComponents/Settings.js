import React, {useEffect, useState} from "react";
import EditableTable from "../SmallComponents/EditableTable";
import Button from "@material-ui/core/Button";
import "./Settings.css"
import fieldsForTable from "../functions";
import CommonService from "../CommonService";

const setting = [{id:1, zone: 'Cucumbers', watering: "5AM, 8 AM, 12AM, 2PM, 5PM, 9PM, 12PM", ventilation: '3AM, 7AM, 4PM, 12PM'},
    {id:1, zone: 'Cucumbers', watering: "5AM, 8 AM, 12AM, 2PM, 5PM, 9PM, 12PM", ventilation: '3AM, 7AM, 4PM, 12PM'},
    {id:1, zone: 'Strawberry', watering: "5AM, 8 AM, 12AM, 2PM, 5PM, 9PM, 12PM", ventilation: '3AM, 7AM, 4PM, 12PM'},
    {id:1, zone: 'Tomatoes', watering: "5AM, 8 AM, 12AM, 2PM, 5PM, 9PM, 12PM", ventilation: '3AM, 7AM, 4PM, 12PM'},
    {id:1, zone: 'Eggplant', watering: "5AM, 8 AM, 12AM, 2PM, 5PM, 9PM, 12PM", ventilation: '3AM, 7AM, 4PM, 12PM'},
    {id:1, zone: 'Cranberry', watering: "5AM, 8 AM, 12AM, 2PM, 5PM, 9PM, 12PM", ventilation: '3AM, 7AM, 4PM, 12PM'},
    {id:1, zone: 'Potato', watering: "5AM, 8 AM, 12AM, 2PM, 5PM, 9PM, 12PM", ventilation: '3AM, 7AM, 4PM, 12PM'},];
let fields = fieldsForTable(setting);

function Settings() {
    const [isEditable, setIsEditable] = useState(false);
    const [settings, setSettings] = useState(setting);
    const [editableRow, setEditableRow] = useState([]);
    const [openModal, setOpenModal] = useState(false);


    const onRowEdit = (row)=>{
        setEditableRow(row);
        setOpenModal(true);
    }

    const handleClose = () => {
        setOpenModal(false);
    };

    useEffect(()=>{
        CommonService.getAll('/settings')
            .then(response => {
                setSettings(response.data);
                fields = fieldsForTable(response.data);
            })
            .catch(e => {
                console.log(`ERROR: ${e}`);
            });
    })
    return (
        <>
            <div className="Container">
            <Button  className="EditButton" color="primary" onClick={(e)=>{
                e.preventDefault();
                setIsEditable(!isEditable)
            }}>
                {isEditable? "Stop editing":"Edit"}
            </Button>
            <EditableTable
            rows={settings}
            fields={fields}
            isEditable={isEditable}
            onRowEdit={onRowEdit}
            />
            </div>
        </>
    )
}

export default Settings;
