import './App.css';
import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Home from "./MainComponents/Home";
import Settings from "./MainComponents/Settings";
import Info from "./MainComponents/Info";
import SettingsIcon from '@material-ui/icons/Settings';
import Log from "./MainComponents/Log";
import Header from "./SmallComponents/Header";
import Footer from "./SmallComponents/Footer";
import InfoIcon from '@material-ui/icons/Info';
import HomeIcon from '@material-ui/icons/Home';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Zones from "./MainComponents/Zones";
import Sensors from "./MainComponents/Sensors";

function App() {
  return (
      <>
        <Header />
        <Router>
          <div className='App'>
            <ul className='Menu-nav'>
              <li>
                <Link to="/" >
                  <div className="Link">
                    <HomeIcon htmlColor={'#282c34'}/>
                    <div>Home</div>
                  </div>
                </Link>
              </li>
              <li>
                <Accordion className="Accordion">
                  <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                  Settings
                  </AccordionSummary>
                  <AccordionDetails className="Details">
                    <Link to="/actualSettings">
                      <div className="Link">
                        <SettingsIcon htmlColor={'#282c34'}/>
                        <div className="sub">Actual settings</div>
                      </div>
                    </Link>
                    <Link to="/log">
                      <div className="Link">
                        <SettingsIcon htmlColor={'#282c34'}/>
                        <div className="sub">Log</div>
                      </div>
                    </Link>
                    <Link to="/sensors">
                      <div className="Link">
                        <SettingsIcon htmlColor={'#282c34'}/>
                        <div className="sub">Sensors</div>
                      </div>
                    </Link>
                  </AccordionDetails>
                </Accordion>
              </li>
              <li>
                <Accordion className="Accordion">
                  <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                    Info
                  </AccordionSummary>
                  <AccordionDetails className="Details">
                    <Link to="/info">
                      <div className="Link">
                        <InfoIcon htmlColor={'#282c34'}/>
                        <div className="sub">All data</div>
                      </div>
                    </Link>
                    <Link to="/zones">
                      <div className="Link">
                        <InfoIcon htmlColor={'#282c34'}/>
                        <div className="sub">Zones</div>
                      </div>
                    </Link>
                  </AccordionDetails>
                </Accordion>

              </li>
            </ul>

            <Switch>
              <Route path="/actualSettings">
                <Settings />
              </Route>
              <Route path="/log">
                <Log />
              </Route>
              <Route path="/sensors">
                <Sensors />
              </Route>
              <Route path="/info">
                <Info />
              </Route>
              <Route path="/zones">
                <Zones />
              </Route>
              <Route path="/">
                <Home />
              </Route>
            </Switch>
          </div>
        </Router>
        <Footer />
      </>
  )
}

export default App;
