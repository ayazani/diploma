export default function fieldsForTable (rows) {
    const fields = [];
    for (let key in rows[0]) {
        fields.push(key);
    }
    return fields;
}
