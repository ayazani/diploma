import 'date-fns';
import React from 'react';
import Grid from '@material-ui/core/Grid';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardTimePicker,
    KeyboardDatePicker,
} from '@material-ui/pickers';

export default function MaterialUIPickers(props) {
    const {isTimePickerNeeded, isModal, date, onDateChange} = props;
    const [selectedDate, setSelectedDate] = React.useState(date);

    const handleDateChange = (date) => {
        setSelectedDate(date);
        onDateChange(date)
    };



    return (
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <Grid container justify="space-around">
                {isModal? <KeyboardDatePicker
                    margin="normal"
                    id="date-picker-dialog"
                    label="Choose date"
                    format="MM/dd/yyyy"
                    value={selectedDate}
                    onChange={handleDateChange}
                    KeyboardButtonProps={{
                        'aria-label': 'change date',
                    }}
                /> : <KeyboardDatePicker
                    disableToolbar
                    variant="inline"
                    format="MM/dd/yyyy"
                    margin="normal"
                    id="date-picker-inline"
                    label="Choose date"
                    value={selectedDate}
                    onChange={handleDateChange}
                    KeyboardButtonProps={{
                        'aria-label': 'change date',
                    }}
                />}
                {isTimePickerNeeded ? <KeyboardTimePicker
                    margin="normal"
                    id="time-picker"
                    label="Choose time"
                    value={selectedDate}
                    onChange={handleDateChange}
                    KeyboardButtonProps={{
                        'aria-label': 'change time',
                    }}
                />: null}
            </Grid>
        </MuiPickersUtilsProvider>
    );
}
