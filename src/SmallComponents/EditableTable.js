import Table from "@material-ui/core/Table";
import './EditableTable.css';
import {
    TableHead,
    TableFooter,
    TablePagination,
    TableRow,
    TableCell,
    TableBody,
    Paper,
    TableContainer, Button, IconButton
} from "@material-ui/core";
import React from "react";
import DeleteIcon from '@material-ui/icons/Delete';

function EditableTable (props) {
    const {rows, isEditable, fields, onRowEdit} = props;
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState((rows.length < 5 ) ? rows.length : 5);
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);
    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const handleRowChange = (row) => {
        onRowEdit(row);
    }
    return(
        <TableContainer component={Paper} className="Table">
            <Table className="" aria-label="custom pagination table">
                 <TableHead>
                     <TableRow>
                         {fields.map((field) => (
                              <TableCell key={field}>{field === "id" ? "№" : field || field.includes('_')? field : field.toString().replace('_', ' ')}</TableCell>
                         ))}
                     </TableRow>
                </TableHead>
                <TableBody>
                    {(rowsPerPage > 0
                        ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        : rows).map((row) => (
                        <TableRow key={row.id}>
                            {fields.map((field) => (
                                <TableCell key={row[field]}>{row[field]}</TableCell>
                            ))}
                            {isEditable?
                                <TableCell key={"edit"}>
                                    <Button onClick={(e)=>{handleRowChange(row)}}> Edit </Button>
                                </TableCell>
                                : null}
                            {isEditable?
                                <TableCell key={"delete"}>
                                    <IconButton aria-label="delete">
                                        <DeleteIcon fontSize="small"/>
                                    </IconButton>
                                </TableCell>
                                : null}

                        </TableRow>
                    ))}

                    {emptyRows > 0 && (
                        <TableRow style={{ height: 53 * emptyRows }}>
                            <TableCell colSpan={6} />
                        </TableRow>
                    )}
                </TableBody>
                <TableFooter>
                    <TableRow>
                        <TablePagination
                            rowsPerPageOptions={[5, 10, { label: 'All', value: -1 }]}
                            colSpan={3}
                            count={rows.length}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            SelectProps={{
                                inputProps: { 'aria-label': 'rows per page' },
                                native: true,
                            }}
                            onChangePage={handleChangePage}
                            onChangeRowsPerPage={handleChangeRowsPerPage}
                            className="Pagination"
                        />
                    </TableRow>
                </TableFooter>
            </Table>
        </TableContainer>
    )
}

export default EditableTable;
