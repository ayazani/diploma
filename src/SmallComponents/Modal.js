import Modal from '@material-ui/core/Modal';
import { makeStyles } from '@material-ui/core/styles';
import React from "react";


function Modalochka (props) {
    const [modalStyle] = React.useState(getModalStyle);
    const {handleClose, handleOpen, open, body, width, height} = props;

    function getModalStyle() {
        const top = 50;
        const left = 50;

        return {
            top: `${top}%`,
            left: `${left}%`,
            transform: `translate(-${top}%, -${left}%)`,
        };
    }

    const useStyles = makeStyles((theme) => ({
        paper: {
            position: 'absolute',
            width: width,
            height: height,
            backgroundColor: 'white',
            border: '2px solid #282c34',
            boxShadow: theme.shadows[5],
            padding: theme.spacing(2, 4, 3),
            color: '#282c34',
        },
    }));
    const classes = useStyles();
    return(
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
        >
            <div style={modalStyle} className = {classes.paper}>{body}</div>
        </Modal>
    )
}
export default Modalochka;
