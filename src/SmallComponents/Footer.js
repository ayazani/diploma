import "./Footer.css"

function Footer () {
    return (
        <footer className="Footer">
            ETO DIPLOM &copy; Anfisa Kartashova & Ivan Koloyanov & Ivanov Alexey
        </footer>
    )
}

export default Footer;
