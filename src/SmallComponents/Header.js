import EmojiNatureIcon from '@material-ui/icons/EmojiNature';
import './Header.css'
import React from "react";

function Header () {
    return(
        <>
            <header className="header">
            <EmojiNatureIcon />
            <div>Greenhouse</div>
            </header>
        </>
    )
}
export default Header;
