import React from "react";
import {
    LineChart,
    Line,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip
} from "recharts";
import {Container, Typography} from "@material-ui/core";

function IlluminanceChart (props){
    const {data, width, height} = props;
    return (
        <Container>
            <Typography variant="h3" component="h5">
                Illuminance
            </Typography>
            <LineChart
                width={width}
                height={height}
                data={data}
                margin={{
                    top: 5,
                    right: 30,
                    left: 20,
                    bottom: 5
                }}
            >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="time" />
                <YAxis />
                <Tooltip />
                <Line
                    type="monotone"
                    dataKey="kilolux"
                    stroke="#ffb73b"
                    strokeWidth={2}
                    activeDot={{ r: 8 }}
                />
            </LineChart>
        </Container>
    )
}

export default IlluminanceChart;
