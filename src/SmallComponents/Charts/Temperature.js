import React from "react";
import {
    LineChart,
    Line,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip
} from "recharts";
import {Container, Typography} from "@material-ui/core";

function TemperatureChart (props){
   const {data, width, height} = props;
   return (
       <Container>
           <Typography variant="h3" component="h5">
               Temperature
           </Typography>
           <LineChart
               width={width}
               height={height}
               data={data}
               margin={{
                   top: 5,
                   right: 30,
                   bottom: 5
               }}
           >
               <CartesianGrid strokeDasharray="3 3" />
               <XAxis dataKey="time" />
               <YAxis />
               <Tooltip />
               <Line
                   type="monotone"
                   dataKey="temperature °C"
                   stroke="#b00d02"
                   strokeWidth={2}
                   activeDot={{ r: 8 }}
               />
           </LineChart>
       </Container>
   )
}

export default TemperatureChart;
