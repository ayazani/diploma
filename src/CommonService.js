import http from "../src/http-common";

class ApiService {
    getAll(endpoint) {
        return http.get(endpoint);
    }

    get(endpoint, id) {
        return http.get(`${endpoint}/${id}`);
    }

    create(endpoint, data) {
        return http.post(`${endpoint}`, data);
    }

    update(endpoint, id, data) {
        return http.put(`${endpoint}/${id}`, data);
    }

    delete(endpoint, id) {
        return http.delete(`${endpoint}/${id}`);
    }

    deleteAll(endpoint) {
        return http.delete(`${endpoint}`);
    }

    findBy(endpoint, dataName, data) {
        return http.get(`${endpoint}?${dataName}=${data}`);
    }
}

export default new ApiService();
